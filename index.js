const _ = require('lodash');
const fs = require('fs');

let files = fs.readdirSync('./lib');
const lib = {}
_.forEach(files, (file) => {
	if(~file.indexOf('.js')){
		let objName = _.camelCase(file.replace(/\.[^/.]+$/, ""));
		lib[objName] = require(`./lib/${file}`);
	}
})
module.exports = lib;