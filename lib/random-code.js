const fs = require('fs');
const path = require('path');

const generateCode = function generateCode() {
	let prom = new Promise ((resolve, reject) => {
		let randomCode = generateRandomCode();
		const dataFilePath = path.resolve(path.join(__dirname, '../sample-data/randomCodes.json'));


		fs.readFile(dataFilePath, 'utf8', (err, content) => {
			let codes = JSON.parse(content);
			if(~codes.indexOf(randomCode)) {
				return generateCode().then((code) => {
					resolve(code);
				});
			}
			resolve(randomCode);

		})
	})
	return prom;
}
function generateRandomCode() {
	return Math.floor(Math.random() * 100);
}
module.exports = generateCode;
