module.exports = function circularShift(arr, rounds) {
	var n = rounds % arr.length;
	var c = arr.slice(arr.length - n, arr.length);
	arr = arr.slice(0, arr.length - n);
	return c.concat(arr);
}